---
title: Configuration
metaTitle: Configuration of a Psono SaaS instance | Psono Documentation
meta:
  - name: description
    content: Instructions how to configure a Psono SaaS instance
---

# Configuration

## Preamble

We assume that you already have provisioned a Psono SaaS instance, if not please create one.

## Guide

1)  Go to "Psono SaaS" and click the pencil icon button

    ![Step 1 Go to "Psono SaaS" and click the "+"](/images/console/psono-saas-instance/creation-psono-saas-instance.png)

2)  Configure `settings.yaml`

    You can now modify all the settings according to your requirements. The settings look "cryptic" yet "map" to the
    settings of the regular settings of the regular `settings.yaml`. Details about each can be found in the regular
    Admin documentation here e.g. [/admin/overview/summary.html](/admin/overview/summary.html).
    
    ![Step 2 Modify settings.yaml](/images/console/psono-saas-instance/configuration-psono-saas-instance-settings.png)

3)  Configure `config.json`

    Some of the documents require that you modify a `config.json`. There are two of those `config.json`, one for the regular
    webclient and one for the portal. You will find these settings at the end as shown in the screenshot below.
    
    ![Step 3 Modify the config.json](/images/console/psono-saas-instance/configuration-psono-saas-instance-config-json.png)



