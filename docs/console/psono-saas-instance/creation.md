---
title: Creation
metaTitle: Creation of a Psono SaaS instance | Psono Documentation
meta:
  - name: description
    content: Instructions how to create a Psono SaaS instance
---

# Creation

## Preamble

In order to create a SaaS instance you need to have a console account with a project and billing account. If you try to
create a SaaS instance without a project or billing account the console will guide you through the process.

## Guide

1)  Go to "Psono SaaS" and click the "+":

    ![Step 1 Go to "Psono SaaS" and click the "+"](/images/console/psono-saas-instance/creation-psono-saas-instance.png)

2)  Fill in the details and select the user amount:

    ![Step 2 Fill in the details](/images/console/psono-saas-instance/creation-psono-saas-instance-fill-details.png)

3)  Wait for the provisioning to complete:
    
    ![Step 2 Fill in the details](/images/console/psono-saas-instance/creation-psono-saas-instance-wait-for-provisioning.png)
    
    ::: tip
    Please take note that the initial provisioning takes about 20 minutes and you need to manually refresh the page for an updated provisioning status.
    If the provisioning takes significantly longer, please reach out to support@psono.com.
    :::

4)  Click the pencil icon button
    
    Once the provisioning is done click on the pencil icon.
    
    ![Step 4 Click on pencil](/images/console/psono-saas-instance/creation-psono-saas-instance-edit.png)

5)  Connection details

    At the top you will see the url of your webclient and the url of your admin portal to manage your users. In addition
    a default user has been created which you can use to login.

    ![Step 5 Connection details](/images/console/psono-saas-instance/creation-psono-saas-instance-connection-details.png)
    
    ::: warning
    Please either change the password of that admin user or delete it before you store any sensitive information.
    :::