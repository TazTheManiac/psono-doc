---
title: Docker secrets
metaTitle: Docker secrets | Psono Documentation
meta:
- name: description
  content: Guide how to use docker secrets with Psono
---

# Docker Secrets

Docker swam supports so called [docker secrets](https://docs.docker.com/engine/swarm/secrets/) to centrally manage sensitive information like passwords
and configurations. Psono supports it to store certain configuration parameters to be managed like that.

## List of supported docker secrets

At the moment the following docker secrets can be configured.

### `SECRET_KEY`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_SECRET_KEY_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_SECRET_KEY -
docker service create ... -e PSONO_SECRET_KEY_FILE=/run/secrets/PSONO_SECRET_KEY ...
```



### `PRIVATE_KEY`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_PRIVATE_KEY_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_PRIVATE_KEY -
docker service create ... -e PSONO_PRIVATE_KEY_FILE=/run/secrets/PSONO_PRIVATE_KEY ...
```



### `PUBLIC_KEY`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_PUBLIC_KEY_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_PUBLIC_KEY -
docker service create ... -e PSONO_PUBLIC_KEY_FILE=/run/secrets/PSONO_PUBLIC_KEY ...
```


### `ACTIVATION_LINK_SECRET`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_ACTIVATION_LINK_SECRET_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_ACTIVATION_LINK_SECRET -
docker service create ... -e PSONO_ACTIVATION_LINK_SECRET_FILE=/run/secrets/PSONO_ACTIVATION_LINK_SECRET ...
```



### `DB_SECRET`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_DB_SECRET_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_DB_SECRET -
docker service create ... -e PSONO_DB_SECRET_FILE=/run/secrets/PSONO_DB_SECRET ...
```


### `EMAIL_SECRET_SALT`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_EMAIL_SECRET_SALT_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_EMAIL_SECRET_SALT -
docker service create ... -e PSONO_EMAIL_SECRET_SALT_FILE=/run/secrets/PSONO_EMAIL_SECRET_SALT ...
```


### `EMAIL_HOST_USER`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_EMAIL_HOST_USER_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_EMAIL_HOST_USER -
docker service create ... -e PSONO_EMAIL_HOST_USER_FILE=/run/secrets/PSONO_EMAIL_HOST_USER ...
```


### `EMAIL_HOST_PASSWORD`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_EMAIL_HOST_PASSWORD_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_EMAIL_HOST_PASSWORD -
docker service create ... -e PSONO_EMAIL_HOST_PASSWORD_FILE=/run/secrets/PSONO_EMAIL_HOST_PASSWORD ...
```


### `DATABASES_DEFAULT_USER`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_DATABASES_DEFAULT_USER_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_DATABASES_DEFAULT_USER -
docker service create ... -e PSONO_DATABASES_DEFAULT_USER_FILE=/run/secrets/PSONO_DATABASES_DEFAULT_USER ...
```


### `DATABASES_DEFAULT_PASSWORD`

If you want to manage this configuration parameter with docker secrets you need to pass an environment variable `PSONO_DATABASES_DEFAULT_PASSWORD_FILE` that contains the path to the file that will contain the secret data. e.g.

```bash
docker secret create PSONO_DATABASES_DEFAULT_PASSWORD -
docker service create ... -e PSONO_DATABASES_DEFAULT_PASSWORD_FILE=/run/secrets/PSONO_DATABASES_DEFAULT_PASSWORD ...
```
