---
title: Introduction
metaTitle: Introduction | Psono Documentation
---

# Introduction

This site provides documentation, training, and other notes for the Psono Password Manager. There's a lot of information about how to do a variety of things.

The instructions here are geared towards:

* End-users, who want some further insight into Psono
* System-administrators, who have to setup and maintain the Psono environment
* Developers who want to contribute

## Getting started

If you do wish to host your own Psono password manager, then please start with the
[installation of the preparations](/admin/installation/install-preparation.html).
