---
title: LDAP Extras
metaTitle: LDAP Extras | Psono Documentation
meta:
  - name: description
    content: Additional configuration details for LDAP
---

# LDAP Extras

## Preamble

The EE server supports the LDAP protocol that allows you to configure an external LDAP service for authentication.
You may configure certain extras, e.g. to restrict access to members of a certain group. This guide will explain how
to do that. We assume that you have a working and running Admin Webclient. If you haven't, please check out the [guide to install the admin client](/admin/installation/install-admin-webclient.html).

::: tip
This feature is only available in the Enterprise Edition.
:::

## Restrict to members of certain LDAP group

You can configure the server to allow only members of a certain group to access and use Psono.

```yaml
LDAP : [
  {
    ...
    'LDAP_REQUIRED_GROUP': ['CN=g1,OU=Groups,OU=example.com,DC=example,DC=com']
    ...
  },
]
```

Restart the server afterwards

::: tip
The filtering is done by Python and as such is case sensitive!
:::

## Restrict to members of certain LDAP group

You can configure the server to allow only members of a certain group to access and use Psono.

```yaml
LDAP : [
  {
    ...
    'LDAP_REQUIRED_GROUP': ['CN=g1,OU=Groups,OU=example.com,DC=example,DC=com']
    ...
  },
]
```

Restart the server afterwards.

::: tip
The filtering is done by Python and as such is case sensitive!
:::

## Restrict to users with a certain attribute

You can configure the server to allow only LDAP users with a certain attribute to access and use Psono. You can either
force the presence of the attribute (use `*` to require only the presence) or require it to be set to a certain value.

```yaml
LDAP : [
  {
    ...
    'LDAP_REQUIRED_ATTRIBUTE': [['displayName', '*']]
    ...
  },
]
```

Restart the server afterwards.

::: tip
The filtering is done by Python and as such is case sensitive!
:::

## Prevent paging

Some servers might not allow LDAP paging. You can disable this 

```yaml
LDAP : [
  {
    ...
    'PAGING_ENABLED': False
    ...
  },
]
```

Restart the server afterwards.