---
title: V16 Files and resources
metaTitle: V16 Files and resources | Psono Documentation
meta:
  - name: description
    content: Files and resources verification requirements
---

# V16 Files and resources

Files and resources verification requirements

[[toc]]

| ID  | Detailed Verification Requirement                                                                                                                                                                                         | Level 1                 | Level 2    | Level 3     | Since     |
|------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|---|-----|
| 16.1 | Verify that URL redirects and forwards only allow whitelisted destinations, or show a warning when redirecting to potentially untrusted content.                                                                                        | x | x | x | 2.0 |
| 16.2 | Verify that untrusted file data submitted to the application is not used directly with file I/O commands, particularly to protect against path traversal, local file include, file mime type, and OS command injection vulnerabilities. | x | x | x | 2.0 |
| 16.3 | Verify that files obtained from untrusted sources are validated to be of expected type and scanned by antivirus scanners to prevent upload of known malicious content.                                                                  | x | x | x | 2.0 |
| 16.4 | Verify that untrusted data is not used within inclusion, class loader, or reflection capabilities to prevent remote/local file inclusion vulnerabilities.                                                                               | x | x | x | 2.0 |
| 16.5 | Verify that untrusted data is not used within cross-domain resource sharing (CORS) to protect against arbitrary remote content.                                                                                                         | x | x | x | 2.0 |
| 16.6 | Verify that files obtained from untrusted sources are stored outside the webroot, with limited permissions, preferably with strong validation.                                                                                          |   | x | x | 3.0 |
| 16.7 | Verify that the web or application server is configured by default to deny access to remote resources or systems outside the web or application server.                                                                                 |   | x | x | 2.0 |
| 16.8 | Verify the application code does not execute uploaded data obtained from untrusted sources.                                                                                                                                             | x | x | x | 3.0 |
| 16.9 | Do not use Flash, Active-X, Silverlight, NACL, client-side Java or other client side technologies not supported natively via W3C browser standards.                                                                                     | x | x | x | 2.0 |


### 16.1 (violation)

The client allows to open any URL as potentially any URL. Doing so otherwise would defeat the whole purpose of a password manager. Showing a warning every time seems to harm the usability too much, as every user needs to confirm before he wants to open the new url.

### 16.2

The fileserver which is the only module that allows files to be uploaded uses Django storages underneath. The path is constructed out of the hash that is generated serverside and then stored on the server. Upon download this stored hash will be used to construct the download path and therefore should not be vulnerable to path traversal

### 16.3 (violation)

All files are encrypted before they are uploaded, therefore neither a mime type check nor an antivirus check is possible.

### 16.4

Untrusted data is not used within inclusion, class loader, or reflection capabilities.

### 16.5

All files are (by default) stored outside of the webroot.

### 16.6

Untrusted data is uploaded encrypted. Endpoints exposing this data (should) run in an own domain context.

### 16.7 (violation)

It is the system administrators responsibility to block outgoing access (e.g. with a firewall). Psono.pw is not doing this.

### 16.8

The application code does not execute any uploaded data.

### 16.9

Neither Flash, Active-X, Silverlight, NACL (Native Client, not NaCl crypto ;D) nor client-side Java is used nor any other client side technology not supported natively via W3C browser standards.
