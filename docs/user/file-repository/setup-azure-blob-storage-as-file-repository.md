---
title: Setup Azure Blob Storage as file repository
metaTitle: Setup Azure Blob Storage as file repository | Psono Documentation
meta:
  - name: description
    content: Instructions how to setup a Azure Blob Storage container as file repository
---

# Setup Azure Blob Storage as file repository

Instructions how to setup a Azure Blob Storage container as file repository


## Pre-requirements

You need to have an Azure account. If not, you can register here [azure.microsoft.com](https://azure.microsoft.com/)
As a new customer Microsoft will provide you with 200 USD for the first 12 months and a lot of other benefits forever free,
e.g. 5 GB of Blob Storage. More details can be found here: [azure.microsoft.com/en-us/free/](https://azure.microsoft.com/en-us/free/)

Further we assume that you have a resource group (if not create one as described here [docs.microsoft.com/en-us/azure/azure-resource-manager/management/manage-resource-groups-portal](https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/manage-resource-groups-portal#create-resource-groups)) and a storage account (if not create one as described here [docs.microsoft.com/en-us/azure/storage/common/storage-account-create?tabs=azure-portal](https://docs.microsoft.com/en-us/azure/storage/common/storage-account-create?tabs=azure-portal#create-a-storage-account-1)).


## Setup Guide

This guide will walk you through the creation of a bucket, the configuration of the bucket and the creation of a
service account, before it helps you to configure it in psono.

### Create container

1) Login to [azure.microsoft.com](https://azure.microsoft.com/)

2) Select your storage account

3) Create container

On the left side select "Container" and then click the "+ Container".

![Step 3 Create container](/images/user/file_repository_setup_azure_blob/azure_blob_create_container.png)

4) Name container

Name the container and click "Create"

![Step 4 Name container](/images/user/file_repository_setup_azure_blob/azure_blob_name_container.png)

::: tip
Remember the container name. You will need it later.
:::

### Configure CORS

5) Click at the left "Resource sharing (CORS)"

And configure CORS like:

![Step 5 Configure CORS](/images/user/file_repository_setup_azure_blob/azure_blob_configure_cors.png)

The configuration should look like this:

- Allowed origins: The url of your Psono instance
- Allowed methods: DELETE, GET, HEAD, OPTIONS, PUT
- Allowed headers: content-type,cache-control,if-modified-since,pragma,x-ms-*
- Exposed headers: x-ms-*
- Max Age: 5 (for testing purposes, you can increase it later)


### Grab access key

6) Go to "Access keys"

In the menu on the left click on "Access keys".

7) Click "Show keys"

![Step 7 Grab access key](/images/user/file_repository_setup_azure_blob/azure_blob_grab_access_key.png)

::: tip
Take a note of the key of key1 (primary key) and the storage account name, you will need it later.
:::

### Configure the file repository


7) Login to Psono

![Step 6 Login to Psono](/images/user/file_repository_setup_gcs/step13-login-to-psono.jpg)

8) Go to "Other"

![Step 7 Go to other](/images/user/file_repository_setup_gcs/step14-go-to-other.jpg)

9) Go to "File Repositories" and click "Create new file repository"

![Step 8 Go to "File Repositories" and click "Create new file repository"](/images/user/file_repository_setup_gcs/step15-select-file-repository-and-click-create-new-file-repository.jpg)

10) Configure the file repository

Use any descriptive title, select Azure Blob Storage as type, add your account name, primary key and container name.

![Step 10 Configure repository](/images/user/file_repository_setup_azure_blob/azure_blob_configure_repository.png)

You can now upload files from the datastore to this file repository.


