---
title: Searching
metaTitle: Searching secrets and folders | Psono Documentation
meta:
  - name: description
    content: This guide will explain how to search efficient for secrets and folders
---

# Searching secrets

This section will explain how to search for a particular secrets. A secret can be a website password, or a note or bookmark.
We assume that you are already logged in to Psono and want to search now a secret. There are multiple methods to
search now an entry.

## In-app search

You can use Psono's powerful in-app search at the top of the datastore.

![In-app search](/images/user/basic/in-app-search.jpg)


## Panel Search

If you are using Psono's browser extension you can use the panel search next to the browsers' address bar.

![Panel search](/images/user/basic/panel-search.jpg)



## Browser address bar search

If you are using Psono's browser extension you can use the address bar search. Start by typing `pp` followed by your search term e.g. `pp gitlab`

![Browser bar search](/images/user/basic/browser-bar-search.jpg)

The first finding will open the datastore with the searched term being passed along. The other entries that follow will
open directly the web page that you searched for if the corresponding search was something with an url, e.g. a website password or a bookmark.