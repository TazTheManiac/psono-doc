---
title: Overview
metaTitle: psonoci Overview | Psono Documentation
meta:
  - name: description
    content: Overview of psonoci.
---

# psonoci Overview

## Preamble

psonoci is the Psono CI / CD integration utility. The client is curently in beta.

## Overview

API which is used for `psonoci` is sessionless with local decryption (see
[API key overview](/user/api-key/overview.html) for details).
